
$(".dropdown-menu").mouseenter(function(){
  $(this).parent().addClass('hvr-active');
});
$(".dropdown-menu").mouseleave(function(){
    $(this).parent().removeClass('hvr-active');
});


   

$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $(".main-header").addClass("header-sticky");
    }
    else{
        $(".main-header").removeClass("header-sticky");
    }
   
});


// Menu Toggle
$('#nav-icon3').click(function(){
    $(this).toggleClass('open');
});
// Menu Toggle



// Project Slider
$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});
// Project Slider




if($(window).width() <= 768){
    if(('.projects-box-slider').length != 0){
        $('.projects-box-slider').addClass('owl-carousel owl-theme');
        $('.projects-box-slider').owlCarousel({
            loop:true,
            margin:0,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: false,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                }, 
                480:{
                    
                    items:1,
                    nav:true,
                },
                577:{
                    
                    items:1,
                    nav:true,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}



// $('.navbar-toggler').click(function(){
//     $('.side-nav-menu .responsive-menu').addClass('active');
//     $('.side-nav-menu .responsive-menu').parent().addClass('overlay-menu');
// });

// $('.close-menu-respon').click(function(){
//     $('.side-nav-menu .responsive-menu').removeClass('active');
//     $('.side-nav-menu .responsive-menu').parent().removeClass('overlay-menu');
// });


// $('.responsive-menu .navbar-nav .nav-item .nav-link').click(function(){
//     $('.side-nav-menu .responsive-menu').parent().removeClass('overlay-menu');
//     $('.side-nav-menu .responsive-menu').removeClass('active');
// });

$('.navbar-toggler').click(function(){
    $('.responsive-menu').addClass('active');
    $('.close-menu-respon').show(500);
    $('.side-nav-menu').addClass('overlay-menu');
});
$('.close-menu-respon').click(function(){
    $('.responsive-menu').removeClass('active');
    $('.close-menu-respon').hide(500);
    $('.side-nav-menu').removeClass('overlay-menu');
});
$('#navbarSupportedContent .nav-item a').on('click', function(){
    $('#navbarSupportedContent .nav-item a.current').removeClass('current');
    $(this).addClass('current');
});
